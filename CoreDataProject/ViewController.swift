//
//  ViewController.swift
//  CoreDataProject
//
//  Created by formador on 13/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var addressTxt: UITextField!
    @IBOutlet weak var eployeeNameTxt: UITextField!
    @IBOutlet weak var employeesTableView: UITableView!
    
    private let employeeCellIdentifier = "employeeCellIdentifier"
    
    private var company: Company? {
        didSet {
            nameTxt.text = company?.name
            addressTxt.text = company?.address
            
            loadEmployees()
        }
    }
    
    private var employees: [Employee]?
    
    private var managedObjectContext: NSManagedObjectContext? {
        
        let appDelegate = (UIApplication.shared.delegate as? AppDelegate)
        
        return appDelegate?.persistentContainer.viewContext
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Asignando el delegado por código
        nameTxt.delegate = self
        addressTxt.delegate = self
        eployeeNameTxt.delegate = self
        
        employeesTableView.dataSource = self
        
        employeesTableView.register(UITableViewCell.self, forCellReuseIdentifier: employeeCellIdentifier)
        
        loadCompany()
    }
    
    
    @IBAction func saveButtonAction(_ sender: Any) {
        
        saveCompany()
    }
    
    private func saveCompany() {
        
        guard let managedObjectContext = managedObjectContext else { return }
        
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
                
                loadCompany()
            }catch {
                print("Error al guardar datos \(error.localizedDescription)")
            }
        }
    }

    private func loadCompany() {
        
        guard let managedObjectContext = managedObjectContext else { return }

        let companyFecthRequest: NSFetchRequest<Company> = Company.fetchRequest()
        
        let companies = try? managedObjectContext.fetch(companyFecthRequest) as [Company]?
        
        if let companiesOptional = companies, let companies = companiesOptional, companies.count >= 1 {
            
            company = companies[0]
        } else {
            
            company = Company(context: managedObjectContext)
        }
        
        employeesTableView.reloadData()
    }
    
    private func loadEmployees() {
        
        let firstNameSortDescriptor = NSSortDescriptor(key: "name", ascending: true)

        employees = company?.emplyees?.sortedArray(using: [firstNameSortDescriptor]) as? [Employee]
    }
    
}

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return company?.emplyees?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: employeeCellIdentifier, for: indexPath)
        
        cell.textLabel?.text = employees?[indexPath.row].name
        
        return cell
    }
    
    
    
}


extension ViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField === nameTxt {
            
            company?.name = textField.text
        } else if textField === addressTxt {
            
            company?.address = textField.text
        } else if textField === eployeeNameTxt {
            
            if let name = textField.text, !name.isEmpty {
                
                guard let managedObjectContext = managedObjectContext else { return }
                
                let employee = Employee(context: managedObjectContext)
                
                employee.name = name
                
                company?.addToEmplyees(employee)
                
                saveCompany()
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        view.endEditing(true)
        
        return true
    }
}



////////////// DEMO ////////////

//Provisional a modo de demo
/*
func saveDemoData() {
    
    guard let managedObjectContext = managedObjectContext else { return }
    
    let employeeOne = Employee(context: managedObjectContext)
    
    employeeOne.name = "Juan"
    employeeOne.age = 15
    
    let employeeTwo = Employee(context: managedObjectContext)
    
    employeeTwo.name = "Pedro"
    
    let company = Company(context: managedObjectContext)
    
    company.name = "CICE"
    company.address = "C/Povedilla 4"
    
    company.addToEmplyees(employeeOne)
    company.addToEmplyees(employeeTwo)
    
    do {
        
        try managedObjectContext.save()
    }catch {
        print("Error al guardar datos \(error.localizedDescription)")
    }
}

func loadDemoData() {
    
    guard let managedObjectContext = managedObjectContext else { return }
    
    let companiesFecth: NSFetchRequest<Company> = Company.fetchRequest()
    companiesFecth.predicate = NSPredicate(format: "address CONTAINS[cd] %@", "Pove")
    
    let companies = try? managedObjectContext.fetch(companiesFecth)
    
    if let companies = companies {
        
        for company in companies {
            
            print("Compañia \(String(describing: company.name)),   dirección \(String(describing: company.address))")
            
            if let employees = company.emplyees {
                
                for employee in employees{
                    
                    print("Empleado \(String(describing: (employee as? Employee)?.name))")
                }
            }
        }
    }
}
*/
